const configs = {
    CALLBACKURL: 'http://localhost:8100',
    PROJECTID:'my-music',
    MONGO_URL: 'mongodb://localhost:27017/mymusic',
    MONGO_OPTIONS: {
        useNewUrlParser: true,
        autoIndex: false, // Don't build indexes
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        reconnectInterval: 500, // Reconnect every 500ms
        poolSize: 10, // Maintain up to 10 socket connections
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0
    },
    VERSION: 'v1',
};
module.exports = configs;

// MONGO_URL: 'mongodb://localhost:27017/mymusic',